import Vue from 'vue'
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import App from './App.vue';
import reservationRoutes from './router/reservation';
import reservation from './store/modules/reservation/';
import kayakRoutes from './router/kayak';
import kayak from './store/modules/kayak/';
import assuranceRoutes from './router/assurance';
import assurance from './store/modules/assurance/';
import clientRoutes from './router/client';
import client from './store/modules/client/';
import assureurRoutes from './router/assureur';
import assureur from './store/modules/assureur/';
Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VueRouter);

export const store = new Vuex.Store({
  // ...
  modules: {
    reservation,
    kayak,
    assurance,
    client,
    assureur,
  }
});

const router = new VueRouter({
  // ...
  routes: [
    ...reservationRoutes,
    ...kayakRoutes,
    ...assuranceRoutes,
    ...clientRoutes,
    ...assureurRoutes,
  ]
});

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
