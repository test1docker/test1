import KayakList from '../components/kayak/List';
import KayakCreate from '../components/kayak/Create';
import KayakUpdate from '../components/kayak/Update';
import KayakShow from '../components/kayak/Show';

export default [
  { name: 'KayakList', path: '/kayaks/', component: KayakList },
  { name: 'KayakCreate', path: '/kayaks/create', component: KayakCreate },
  { name: 'KayakUpdate', path: '/kayaks/edit/:id', component: KayakUpdate },
  { name: 'KayakShow', path: '/kayaks/show/:id', component: KayakShow },
]
