import AssureurList from '../components/assureur/List';
import AssureurCreate from '../components/assureur/Create';
import AssureurUpdate from '../components/assureur/Update';
import AssureurShow from '../components/assureur/Show';

export default [
  { name: 'AssureurList', path: '/assureurs/', component: AssureurList },
  { name: 'AssureurCreate', path: '/assureurs/create', component: AssureurCreate },
  { name: 'AssureurUpdate', path: '/assureurs/edit/:id', component: AssureurUpdate },
  { name: 'AssureurShow', path: '/assureurs/show/:id', component: AssureurShow },
]
