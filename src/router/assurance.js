import AssuranceList from '../components/assurance/List';
import AssuranceCreate from '../components/assurance/Create';
import AssuranceUpdate from '../components/assurance/Update';
import AssuranceShow from '../components/assurance/Show';

export default [
  { name: 'AssuranceList', path: '/assurances/', component: AssuranceList },
  { name: 'AssuranceCreate', path: '/assurances/create', component: AssuranceCreate },
  { name: 'AssuranceUpdate', path: '/assurances/edit/:id', component: AssuranceUpdate },
  { name: 'AssuranceShow', path: '/assurances/show/:id', component: AssuranceShow },
]
