export const KAYAK_SHOW_RESET = 'KAYAK_SHOW_RESET';
export const KAYAK_SHOW_SET_ERROR = 'KAYAK_SHOW_SET_ERROR';
export const KAYAK_SHOW_SET_RETRIEVED = 'KAYAK_SHOW_SET_RETRIEVED';
export const KAYAK_SHOW_TOGGLE_LOADING = 'KAYAK_SHOW_TOGGLE_LOADING';
