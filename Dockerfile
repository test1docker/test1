#FROM node:lts-alpine
## install simple http server for serving static content
#RUN npm install -g http-server
## make the 'app' folder the current working directory
#WORKDIR /app
## copy both 'package.json' and 'package-lock.json' (if available)
#COPY package*.json ./
#
## install project dependencies
#RUN npm install
#
## copy project files and folders to the current working directory (i.e. 'app' folder)
#COPY . .
#
## build app for production with minification
#RUN npm run build
#COPY /web dist/
#COPY index.html dist/
#EXPOSE 5000
#CMD [ "http-server", "-p 5000", "dist" ]
#

# étape de build
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# étape de production
FROM nginx:stable-alpine as production-stage
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/dist /usr/share/nginx/html
#COPY --from=build-stage /app/index.html /usr/share/nginx/html
COPY --from=build-stage /app/web /usr/share/nginx/html
EXPOSE 5000
CMD ["nginx", "-g", "daemon off;"]
